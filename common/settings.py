import os
from django.contrib.messages import constants as messages


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Change these variables in production ---------------------------------------
DEBUG = True
ALLOWED_HOSTS = []
SECRET_KEY = "8w8(^=$v)2k!$5d3tbgx0o@i*af__74%fnaa^(yv^#vp))ti+o"
# ----------------------------------------------------------------------------

ROOT_URLCONF = "common.urls"
WSGI_APPLICATION = "common.wsgi.application"

LANGUAGE_CODE = "en-us"
TIME_ZONE = "America/New_York"
USE_I18N = False
USE_L10N = False
USE_TZ = False

STATIC_URL = "/static/"
MEDIA_URL = "/media/"
LOGIN_URL = "/account/login/"
if DEBUG:
    MEDIA_ROOT = os.path.join(BASE_DIR, "media")
else:
    STATIC_ROOT = "/var/www/the-blog/static/"
    MEDIA_ROOT = "/var/www/the-blog/media/"

AUTH_USER_MODEL = "account.Account"
CRISPY_TEMPLATE_PACK = "bootstrap4"


RESTRUCTUREDTEXT_FILTER_SETTINGS = {"syntax_highlight": "short", "doctitle_xform":False}

DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": os.path.join(BASE_DIR, "blog.sqlite3")}}

MESSAGE_TAGS = {
    messages.DEBUG: "dark",
    messages.INFO: "info",
    messages.SUCCESS: "success",
    messages.WARNING: "warning",
    messages.ERROR: "danger",
}

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "crispy_forms",
    "common",
    "account",
    "blog",
    "boxes",
    "django_cleanup.apps.CleanupConfig",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "blog.context_processors.posts",
                "blog.context_processors.years",
            ],
        },
    },
]
