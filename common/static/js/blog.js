$(document).ready(function () {
    $('.sidenav > div > a[href="' + window.location.pathname + '"]').addClass('active');
    setTimeout(function () {
        $(".alert-dismissible").alert("close");
    }, 5000);


    if ($('input[name="tags_list"]').length) {
        $('input[name="tags_list"]').amsifySuggestags({
            type: 'bootstrap',
        });
    }

    if ($('a[href*=":download:"').length) {
        $('a[href*=":download:"').attr("download", true);
        $('a[href*=":download:"').each(function () {
            this.href = this.href.replace(':download:', '');
        });
    }

    loadPreview();
    $('textarea[name="content"]').on("input", loadPreview);
});

function loadPreview() {
    $.ajax({
        url: "/blog/render/",
        type: "post",
        data: { content: $('textarea[name="content"]').val() },
        success: function (result) {
            $("#preview").html(result);
        }
    });
}

function genPDF() {
    $("img").css({ "width": "75px", "align": "left" });
    var margin = {
        top: 15,
        left: 10,
        right: 0,
        bottom: 15
    };

    var doc = new jsPDF("portrait", "mm", "letter");
    doc.fromHTML($('#pdf').get(0), 15, 15,
        { 'width': 175 },
        function (bla) { doc.save('wiki.pdf'); location.reload(); },
        margin);
}
