from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from django.views.generic.base import RedirectView, TemplateView


urlpatterns = [
    path("", TemplateView.as_view(template_name="index.html"), name="index"),
    path("account/", include("account.urls", namespace="account")),
    path("blog/", include("blog.urls", namespace="blog")),
    path("boxes/", include("boxes.urls", namespace="boxes")),
    path("about_me/", TemplateView.as_view(template_name="about.html"), name="about_me"),
    path("login/", RedirectView.as_view(url="/account/login/", permanent=False)),
    path("logout/", RedirectView.as_view(url="/account/login/", permanent=False)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
