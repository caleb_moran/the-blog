from django.test import TestCase
from django.urls import resolve


class BoxesUrlsCase(TestCase):
    def test_edit(self):
        """Check edit URL correct"""
        self.assertEqual(resolve("/boxes/1/").view_name, "boxes:edit")
