from django.test import TestCase
from account.models import Account
from boxes.models import Box


class BoxesEditTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()

    def test_template(self):
        """Check edit template"""
        self.client.force_login(self.superuser)
        response = self.client.get("/boxes/1/", follow=True)
        self.assertTemplateUsed(response, template_name="boxes/edit.html")

    def test_successful(self):
        """Check edit successful"""
        self.client.force_login(self.superuser)
        response = self.client.post(f"/boxes/1/", {"content": "Changed Content"}, follow=True)
        self.assertContains(response, "The box has been successfully edited.")

    def test_no_content(self):
        """No content in box"""
        self.client.force_login(self.superuser)
        response = self.client.post(f"/boxes/1/", {"content": ""}, follow=True)
        self.assertContains(response, "This field is required.")
