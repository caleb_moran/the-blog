from boxes import views
from django.urls import path

app_name = "boxes"

urlpatterns = [
    path("<int:pk>/", views.EditView.as_view(), name="edit"),
]
