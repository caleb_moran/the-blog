from django import template
from django.urls import reverse
from django.utils.safestring import mark_safe
from boxes.models import Box
from blog.templatetags.restruct import restruct

register = template.Library()


@register.simple_tag(takes_context=True)
def box(context, uid, text=False):
    box = Box.objects.get(uid=uid).content

    if text:
        box = restruct(box)

    request = context["request"]
    if request.user.is_authenticated:
        box += f'<a class="btn btn-link" href="{reverse("boxes:edit", args={uid})}?next={request.get_full_path()}"><i style="text-shadow: 0px 0px 8px #ffffff, 0px 0px 8px #ffffff;" class="fas fa-pen fa-fw"></i></a>'

    return mark_safe(box)
