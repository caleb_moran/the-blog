from django.test import TestCase
from blog.apps import BlogConfig


class BlogAppsTestCase(TestCase):
    def test_name(self):
        """Check name correct"""
        self.assertEqual(BlogConfig.name, "blog")
