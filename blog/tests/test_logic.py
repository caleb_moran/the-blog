from django.test import TestCase
from account.models import Account
from blog.models import Post, Tag, Upload
from django.core.files.uploadedfile import SimpleUploadedFile


class SearchTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json", "tests/test_data/blogs.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()
        self.blog = Post.objects.get()

    def test_template(self):
        """Check search template"""
        self.client.force_login(self.superuser)
        response = self.client.get("/blog/", follow=True)
        self.assertTemplateUsed(response, template_name="blog/search.html")

    def test_search(self):
        """Test search by tag"""
        self.client.force_login(self.superuser)
        response = self.client.get("/blog/?search=test", follow=True)
        self.assertContains(response, "Title")
        """Test search by title"""
        response = self.client.get("/blog/?search=Title", follow=True)
        self.assertContains(response, "Title")
        """Test search by content"""
        response = self.client.get("/blog/?search=This+is+some+content", follow=True)
        self.assertContains(response, "Title")
        self.client.logout()
        response = self.client.get("/blog/?search=test", follow=True)
        self.assertContains(response, "Title")
        """Test search by title"""
        response = self.client.get("/blog/?search=Title", follow=True)
        self.assertContains(response, "Title")
        """Test search by content"""
        response = self.client.get("/blog/?search=This+is+some+content", follow=True)
        self.assertContains(response, "Title")

    def test_tag(self):
        """Test search by tag"""
        self.client.force_login(self.superuser)
        response = self.client.get("/blog/?tag=test", follow=True)
        self.assertContains(response, "Title")
        self.client.logout()
        response = self.client.get("/blog/?tag=test", follow=True)
        self.assertContains(response, "Title")

    def test_year(self):
        """Test year search"""
        self.client.force_login(self.superuser)
        response = self.client.get("/blog/?year=2020", follow=True)
        self.assertContains(response, "Title")
        self.client.logout()
        response = self.client.get("/blog/?year=2020", follow=True)
        self.assertContains(response, "Title")


class TagsTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json", "tests/test_data/blogs.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()
        self.blog = Post.objects.get()

    def test_template(self):
        """Check tags template"""
        self.client.force_login(self.superuser)
        response = self.client.get("/blog/tags/", follow=True)
        self.assertTemplateUsed(response, template_name="blog/tags.html")


class CreateTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json", "tests/test_data/blogs.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()
        self.blog = Post.objects.get()

    def test_template(self):
        """Check create template"""
        self.client.force_login(self.superuser)
        response = self.client.get("/blog/create/", follow=True)
        self.assertTemplateUsed(response, template_name="blog/create.html")

    def test_successful_create(self):
        """Check successful create"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/blog/create/",
            {
                "title": "Title",
                "content": "test_user123",
                "tags": "test",
                "created_at": "2020-05-02",
                "is_published": True,
            },
            follow=True,
        )
        self.assertContains(response, "You have successfully created a new blog post.")

    def test_invalid_field(self):
        """Check invalid field"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/blog/create/",
            {"title": "", "content": "test_user123", "tags": "test", "created_at": "2020-05-02", "is_published": True},
            follow=True,
        )
        self.assertContains(response, "This field is required.")


class PostTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json", "tests/test_data/blogs.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()
        self.blog = Post.objects.get()

    def test_template(self):
        """Check post template"""
        self.client.force_login(self.superuser)
        response = self.client.get("/blog/1/", follow=True)
        self.assertTemplateUsed(response, template_name="blog/post.html")


class EditTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json", "tests/test_data/blogs.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()
        self.blog = Post.objects.get()

    def test_template(self):
        """Check edit template"""
        self.client.force_login(self.superuser)
        response = self.client.get("/blog/1/edit/", follow=True)
        self.assertTemplateUsed(response, template_name="blog/edit.html")

    def test_successful_edit(self):
        """Check successful create"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/blog/create/",
            {
                "title": "Title",
                "content": "test_user123",
                "tags": "test",
                "created_at": "2020-05-02",
                "is_published": True,
            },
            follow=True,
        )
        self.assertContains(response, "You have successfully created a new blog post.")
        response = self.client.post(
            "/blog/2/edit/",
            {
                "title": "New Title",
                "content": "test_user123",
                "tags": "test",
                "created_at": "2020-05-02",
                "is_published": True,
            },
            follow=True,
        )
        self.assertContains(response, "You have successfully edited a blog post.")

    def test_invalid_field(self):
        """Check invalid field"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/blog/1/edit/",
            {"title": "", "content": "", "tags": "test", "created_at": "2020-05-02", "is_published": True},
            follow=True,
        )
        self.assertContains(response, "This field is required.")


class DeleteTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json", "tests/test_data/blogs.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()
        self.blog = Post.objects.get()

    def test_template(self):
        """Check edit template"""
        self.client.force_login(self.superuser)
        response = self.client.get("/blog/1/delete/", follow=True)
        self.assertTemplateUsed(response, template_name="blog/delete.html")

    def test_successful_delete(self):
        """Check delete blog"""
        self.client.force_login(self.superuser)
        response = self.client.post("/blog/1/delete/", follow=True)
        self.assertContains(response, "You have successfully deleted a blog post.")


class RenderTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json", "tests/test_data/blogs.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()
        self.blog = Post.objects.get()

    def test_successful_render(self):
        """Check successful create"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/blog/render/",
            {
                "content": "- This is a bullet point",
            },
            follow=True,
        )
        self.assertContains(response, "<li>This is a bullet point</li>")

        
class BoxesUploadTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json", "tests/test_data/blogs.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()
        self.blog = Post.objects.get()
    
    def test_successful(self):
        """Check successful get"""
        self.client.force_login(self.superuser)
        response = self.client.get("/blog/upload/",follow=True)
        self.assertContains(response, "Upload")

    def test_unsuccessful_post(self):
        """check unsuccessful post"""
        self.client.force_login(self.superuser)
        response = self.client.post(f"/blog/upload/", {"file": "uploaded"}, follow=True)
        self.assertContains(response, '{"is_valid": false}')

class BoxesUploadDeleteTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json", "tests/test_data/blogs.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()
        self.blog = Post.objects.get()

    def test_template(self):
        """Check edit template"""
        self.client.force_login(self.superuser)
        smalls_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('smalls.gif', smalls_gif, content_type='image/gif')
        response = self.client.post(f"/blog/upload/", {"file": uploaded}, follow=True)
        response = self.client.get("/blog/upload/1/delete/", follow=True)
        self.assertTemplateUsed(response, template_name="blog/upload_delete.html")

    def test_successful(self):
        """Check successful get"""
        self.client.force_login(self.superuser)
        smalls_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('smalls.gif', smalls_gif, content_type='image/gif')
        response = self.client.post(f"/blog/upload/", {"file": uploaded}, follow=True)
        response = self.client.post(f"/blog/upload/1/delete/",follow=True)
        self.assertContains(response, "You have successfully deleted a file.")
