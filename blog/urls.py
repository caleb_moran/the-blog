from django.urls import path
from blog import views

app_name = "blog"

urlpatterns = [
    path("", views.SearchView.as_view(), name="list"),
    path("tags/", views.TagsView.as_view(), name="tags"),
    path("create/", views.CreateView.as_view(), name="create"),
    path("<int:pk>/", views.PostView.as_view(), name="post"),
    path("<int:pk>/edit/", views.EditView.as_view(), name="edit"),
    path("<int:pk>/delete/", views.DeleteView.as_view(), name="delete"),
    path("render/", views.RenderMarkdown.as_view(), name="render"),
    path("upload/", views.UploadView.as_view(), name="upload"),
    path("upload/<int:pk>/delete/", views.UploadDeleteView.as_view(), name="upload-delete"),
]
