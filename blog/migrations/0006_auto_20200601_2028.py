# Generated by Django 3.0.6 on 2020-06-01 20:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20200601_2005'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='created_at',
            field=models.DateField(help_text='Must be formatted as YYYY-MM-DD'),
        ),
    ]
