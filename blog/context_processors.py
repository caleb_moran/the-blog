from blog.models import Post


def posts(context):
    if context.user.is_authenticated:
        post_list = Post.objects.all()[:4]
    else:
        post_list = Post.objects.filter(is_published=True)[:4]
    return {"posts": post_list}


def years(context):
    year_list = []
    if context.user.is_authenticated:
        for post in Post.objects.all():
            if post.created_at.year not in year_list:
                year_list.append(post.created_at.year)
    else:
        for post in Post.objects.filter(is_published=True):
            if post.created_at.year not in year_list:
                year_list.append(post.created_at.year)
    return {"years": year_list}
