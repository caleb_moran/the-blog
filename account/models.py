from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from account.managers import AccountManager


class Account(AbstractBaseUser):
    uid = models.AutoField(primary_key=True)
    email = models.EmailField(unique=True, error_messages={"unique": "This email is already in use."})
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)

    objects = AccountManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["first_name", "last_name"]

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        return self.first_name + " " + self.last_name
