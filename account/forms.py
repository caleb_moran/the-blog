from django import forms
from django.contrib.auth import authenticate, login
from account.models import Account


class LoginForm(forms.Form):
    email = forms.EmailField(label="Email")
    password = forms.CharField(label="Password", widget=forms.PasswordInput(render_value=False))

    def clean_email(self):
        return self.cleaned_data["email"].lower()

    def clean(self):
        if self._errors:
            return
        self.account = authenticate(email=self.cleaned_data["email"], password=self.cleaned_data["password"])
        if self.account is None:
            raise forms.ValidationError("The email and/or password you entered are incorrect.")
        return self.cleaned_data

    def login(self, request):
        if self.is_valid():
            login(request, self.account)
            return True
        return False


class PasswordChangeForm(forms.Form):
    new_password = forms.CharField(label="New Password", widget=forms.PasswordInput(render_value=False))
    verify_new_password = forms.CharField(label="Verify New Password", widget=forms.PasswordInput(render_value=False))

    def clean(self):
        if self._errors:
            return
        if self.cleaned_data["new_password"] != self.cleaned_data["verify_new_password"]:
            raise forms.ValidationError("The passwords do not match.")
        return self.cleaned_data

    def save(self, account):
        account.set_password(self.cleaned_data["new_password"])
        account.save()
        return account


class AccountForm(forms.ModelForm):
    verify_email = forms.EmailField(label="Verify Email")

    class Meta:
        model = Account
        fields = (
            "email",
            "verify_email",
            "first_name",
            "last_name",
        )
        labels = {
            "first_name": "First Name",
            "last_name": "Last Name",
        }

    def clean_email(self):
        return self.cleaned_data["email"].lower()

    def clean_verify_email(self):
        return self.cleaned_data["verify_email"].lower()

    def clean(self):
        self.cleaned_data = super().clean()
        if self._errors:
            return
        if self.cleaned_data["email"] != self.cleaned_data["verify_email"]:
            raise forms.ValidationError("The emails do not match.")
        return self.cleaned_data
