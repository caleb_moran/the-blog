from django.urls import path
from account import views

app_name = "account"

urlpatterns = [
    path("", views.AccountView.as_view(), name="account"),
    path("edit/", views.EditView.as_view(), name="edit"),
    path("password/change/", views.PasswordChangeView.as_view(), name="password_change"),
    path("login/", views.LoginView.as_view(), name="login"),
    path("logout/", views.LogoutView.as_view(), name="logout"),
]
