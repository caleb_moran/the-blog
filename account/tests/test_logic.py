from django.test import TestCase
from account.models import Account
from account.managers import AccountManager


class AccountsDetailTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()

    def test_successful(self):
        """Check detail successful"""
        self.client.force_login(self.superuser)
        response = self.client.get(f"/account/", follow=True)
        self.assertContains(response, self.superuser.uid)
        self.assertContains(response, self.superuser.first_name)
        self.assertContains(response, self.superuser.last_name)
        self.assertContains(response, self.superuser.email)


class AccountLoginTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()

    def test_template(self):
        """Check login template"""
        response = self.client.get("/account/login/", follow=True)
        self.assertTemplateUsed(response, template_name="account/login.html")

    def test_already_logged_in(self):
        """Check already logged in"""
        self.client.force_login(self.superuser)
        response = self.client.get(f"/account/login/", follow=True)
        self.assertContains(response, "You are already logged in!")

    def test_successful_login(self):
        """Test successful login"""
        response = self.client.post(
            "/account/login/", {"email": self.superuser.email, "password": "test_superuser123"}, follow=True,
        )
        self.assertContains(
            response, f"Welcome back, {self.superuser.first_name}! You have successfully logged in.",
        )

    def test_incorrect_email(self):
        """Check login unsuccessful (email)"""
        response = self.client.post(
            "/account/login/", {"email": self.superuser.email + "1", "password": "test_superuser123"}, follow=True,
        )
        self.assertContains(response, "The email and/or password you entered are incorrect.")

    def test_incorrect_password(self):
        """Check login unsuccessful (email)"""
        response = self.client.post(
            "/account/login/", {"email": self.superuser.email, "password": "test_superuser12311111"}, follow=True,
        )
        self.assertContains(response, "The email and/or password you entered are incorrect.")

    def test_invalid_field(self):
        """Determine if field hasn't been filled"""
        response = self.client.post("/account/login/", {"email": self.superuser.email, "password": ""}, follow=True,)
        self.assertContains(response, "This field is required.")


class AccountLogOutTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()

    def test_successful_logout(self):
        """Successful Logout"""
        self.client.force_login(self.superuser)
        response = self.client.get("/account/logout/", follow=True)
        self.assertContains(response, "You have successfully logged out.")

    def test_already_logged_out(self):
        """Already logged out"""
        self.client.force_login(self.superuser)
        response = self.client.get("/account/logout/", follow=True)
        self.assertContains(response, "You have successfully logged out.")
        self.client.logout()
        response = self.client.get("/account/logout/", follow=True)
        self.assertContains(response, "You are already logged out!")


class AccountPasswordChangeTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()

    def test_template(self):
        """Check login template"""
        self.client.force_login(self.superuser)
        response = self.client.get("/account/password/change/", follow=True)
        self.assertTemplateUsed(response, template_name="account/password/change.html")

    def test_successful_password_change(self):
        """Successful Password Change"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/account/password/change/",
            {"new_password": "test_superuser12311111", "verify_new_password": "test_superuser12311111",},
            follow=True,
        )
        self.assertContains(response, "You have successfully changed your password.")

    def test_passwords_do_not_match(self):
        """Passwords don't match"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/account/password/change/",
            {"new_password": "test_superuser123", "verify_new_password": "test_superuser12311111",},
            follow=True,
        )
        self.assertContains(response, "The passwords do not match.")

    def test_password_field_not_filled(self):
        """Passwords field isn't filled"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/account/password/change/",
            {"new_password": "", "verify_new_password": "test_superuser12311111"},
            follow=True,
        )
        self.assertContains(response, "This field is required.")


class AccountEditTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()

    def test_template(self):
        """Check login template"""
        self.client.force_login(self.superuser)
        response = self.client.get("/account/edit/", follow=True)
        self.assertTemplateUsed(response, template_name="account/edit.html")

    def test_successful_edit(self):
        """Successfully Edit Account"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            f"/account/edit/",
            {"email": "test@test.com", "verify_email": "test@test.com", "first_name": "John", "last_name": "Smith",},
            follow=True,
        )
        self.assertContains(response, "The account has been successfully edited.")

    def test_invalid_field(self):
        """Determine if field hasn't been filled"""
        self.client.force_login(self.superuser)
        response = self.client.post("/account/edit/", {"email": self.superuser.email, "password": ""}, follow=True,)
        self.assertContains(response, "This field is required.")

    def test_email_do_not_match(self):
        """Successfully emails do not match"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            f"/account/edit/",
            {"email": "test@test.com", "verify_email": "test123@test.com", "first_name": "John", "last_name": "Smith",},
            follow=True,
        )
        self.assertContains(response, "The emails do not match.")


class AccountsModelsTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()

    def test_str(self):
        """Check __str__ correct"""
        self.assertEqual(self.superuser.__str__(), "Test User")

    def test_get_full_name(self):
        """Check get_full_name correct"""
        self.assertEqual(self.superuser.get_full_name(), "Test User")


class AccountsManagersTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json", "initial_data.json"]

    def setUp(self):
        """Set up accounts"""
        self.superuser = Account.objects.get()

    def test_create_user(self):
        """Check create_user successful"""
        Account.objects.create_user(
            email="email@example.com", password="normaluser", first_name="Tom", last_name="Jerry",
        )

    def test_create_user_email_incorrect(self):
        """Check create_user fail (no email)"""
        self.assertRaises(
            ValueError,
            Account.objects.create_user,
            email="",
            password="normaluser",
            first_name="Tom",
            last_name="Jerry",
        )

    def test_create_superuser(self):
        """Check create_superuser successful"""
        Account.objects.create_superuser(
            email="email@example.com", password="superuser", first_name="Tom", last_name="Jerry",
        )

    def test_create_superuser_is_incorrect(self):
        """Check create_superuser fail (no email)"""
        self.assertRaises(
            ValueError,
            Account.objects.create_superuser,
            email="",
            password="superuser",
            first_name="Tom",
            last_name="Jerry",
        )
