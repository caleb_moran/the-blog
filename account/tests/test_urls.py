from django.test import TestCase
from django.urls import resolve


class AccountUrlsCase(TestCase):
    def test_detail(self):
        """Check account detail correct"""
        self.assertEqual(resolve("/account/").view_name, "account:account")

    def test_edit(self):
        """Check edit URL correct"""
        self.assertEqual(resolve("/account/edit/").view_name, "account:edit")

    def test_password(self):
        """Check password change URL correct"""
        self.assertEqual(resolve("/account/password/change/").view_name, "account:password_change")

    def test_login(self):
        """Check login URL correct"""
        self.assertEqual(resolve("/account/login/").view_name, "account:login")

    def test_logout(self):
        """Check logout URL correct"""
        self.assertEqual(resolve("/account/logout/").view_name, "account:logout")
